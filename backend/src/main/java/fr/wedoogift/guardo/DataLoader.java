package fr.wedoogift.guardo;

import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.User;
import fr.wedoogift.guardo.repositories.UsersRepository;
import fr.wedoogift.guardo.services.CompaniesService;
import fr.wedoogift.guardo.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationRunner {

    private final UsersService     usersService;
    private final CompaniesService companiesService;

    public void run(ApplicationArguments args) {
        usersService.saveAll(List.of(
                User.builder().id(1L).balance(0).build(),
                User.builder().id(2L).balance(0).build(),
                User.builder().id(3L).balance(0).build()));

        companiesService.saveAll(List.of(
                Company.builder().id(1L).name("Wedoogift").balance(1000).build(),
                Company.builder().id(2L).name("Wedoofood").balance(3000).build()
        ));
    }
}
