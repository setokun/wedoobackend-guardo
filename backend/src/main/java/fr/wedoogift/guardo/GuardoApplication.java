package fr.wedoogift.guardo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuardoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuardoApplication.class, args);
    }

}
