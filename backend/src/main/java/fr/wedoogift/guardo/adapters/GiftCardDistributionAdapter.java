package fr.wedoogift.guardo.adapters;

import fr.wedoogift.guardo.dto.GiftCardDistributionDTO;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class GiftCardDistributionAdapter {

    public GiftCardDistribution adapt(GiftCardDistributionDTO dto) {

        LocalDate startDate = dto.getStartDate();
        if (startDate == null) startDate = LocalDate.now();

        return GiftCardDistribution.builder()
                                   .amount(dto.getAmount())
                                   .startDate(startDate)
                                   .endDate(dto.getEndDate() != null ? dto.getEndDate() : startDate.plusYears(1).minusDays(1))
                                   .companyId(dto.getCompanyId())
                                   .userId(dto.getUserId())
                                   .build();
    }

}
