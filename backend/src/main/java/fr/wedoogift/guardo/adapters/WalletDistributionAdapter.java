package fr.wedoogift.guardo.adapters;

import fr.wedoogift.guardo.dto.WalletDistributionDTO;
import fr.wedoogift.guardo.entities.WalletDistribution;
import fr.wedoogift.guardo.entities.WalletType;
import org.springframework.stereotype.Component;
import fr.wedoogift.guardo.core.utils.*;

import java.time.LocalDate;

@Component
public class WalletDistributionAdapter {

    public WalletDistribution adapt(WalletDistributionDTO dto) {

        LocalDate startDate = dto.getStartDate();
        if (startDate == null) startDate = LocalDate.now();

        LocalDate endDate = DateUtils.getWalletEndDate(WalletType.fromInteger(dto.getWalletId()), startDate);

        return WalletDistribution.builder()
                                 .amount(dto.getAmount())
                                 .walletId(dto.getWalletId())
                                 .startDate(startDate)
                                 .endDate(endDate)
                                 .companyId(dto.getCompanyId())
                                 .userId(dto.getUserId())
                                 .build();
    }

}
