package fr.wedoogift.guardo.controllers;

import fr.wedoogift.guardo.adapters.GiftCardDistributionAdapter;
import fr.wedoogift.guardo.adapters.WalletDistributionAdapter;
import fr.wedoogift.guardo.core.entities.ValidList;
import fr.wedoogift.guardo.dto.GiftCardDistributionDTO;
import fr.wedoogift.guardo.dto.GiftCardEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletDistributionDTO;
import fr.wedoogift.guardo.dto.WalletEndowmentDTO;
import fr.wedoogift.guardo.services.CompaniesService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.NoSuchElementException;

@SuppressWarnings({"rawtypes", "unchecked"})
@RestController
@RequiredArgsConstructor
@RequestMapping("/companies")
@PreAuthorize("isAuthenticated() AND hasAnyRole('COMPANY', 'ADMIN')")
public class CompaniesController {

    private final CompaniesService            companiesService;
    private final GiftCardDistributionAdapter giftCardDistributionAdapter;
    private final WalletDistributionAdapter   walletDistributionAdapter;

    @PostMapping(value = "/distributeGiftCards")
    public ResponseEntity<GiftCardEndowmentDTO> distributeGiftCards(@RequestBody @Valid ValidList<GiftCardDistributionDTO> dto) {
        if (dto == null || dto.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        try {
            GiftCardEndowmentDTO response = companiesService.distributeGiftCards(dto.stream()
                                                                                    .map(giftCardDistributionAdapter::adapt)
                                                                                    .toList());
            if (response == null)
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (NumberFormatException numberFormatException) {
            return new ResponseEntity(numberFormatException.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NoSuchElementException noSuchElementException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/distributeWallets")
    public ResponseEntity<WalletEndowmentDTO> distributeWallets(@Valid @RequestBody ValidList<WalletDistributionDTO> dto) {
        if (dto == null || dto.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        try {
            WalletEndowmentDTO response = companiesService.distributeWallets(dto.stream()
                                                                                .map(walletDistributionAdapter::adapt)
                                                                                .toList());
            if (response == null)
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (NumberFormatException numberFormatException) {
            return new ResponseEntity(numberFormatException.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NoSuchElementException noSuchElementException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}/balance")
    public ResponseEntity<Integer> getBalance(@PathVariable Long id) {
        try {
            int currentBalance = companiesService.getCurrentBalance(id);
            return new ResponseEntity<>(currentBalance, HttpStatus.OK);
        } catch (Exception noSuchElementException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
