package fr.wedoogift.guardo.controllers;

import fr.wedoogift.guardo.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@PreAuthorize("isAuthenticated() AND hasAnyRole('ADMIN', 'USER')")
public class UsersController {

    private final UsersService usersService;

    @GetMapping(value = "/{id}/balance")
    public ResponseEntity<Integer> getBalance(@PathVariable Long id) {
        try {
            int currentBalance = usersService.getCurrentBalance(id);
            return new ResponseEntity<>(currentBalance, HttpStatus.OK);
        } catch (Exception noSuchElementException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
