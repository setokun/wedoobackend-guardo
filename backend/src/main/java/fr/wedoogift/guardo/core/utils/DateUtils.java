package fr.wedoogift.guardo.core.utils;

import fr.wedoogift.guardo.entities.WalletType;

import java.time.LocalDate;
import java.time.Month;

public class DateUtils {

    public static LocalDate getNextYearFebruaryEnd(LocalDate startDate) {
        int year = startDate.getYear() + 1;

        return LocalDate.of(year, Month.MARCH, 1)
                        .minusDays(1);
    }

    public static LocalDate getWalletEndDate(WalletType walletType, LocalDate startDate) {
        //noinspection SwitchStatementWithTooFewBranches
        return switch (walletType) {
            case FOOD -> getNextYearFebruaryEnd(startDate);
            default -> startDate.plusYears(1).minusDays(1);
        };
    }
}
