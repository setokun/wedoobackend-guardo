package fr.wedoogift.guardo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GiftCardDistributionDTO {
    @NotNull
    private int       amount;
    @JsonProperty("start_date")
    private LocalDate startDate;
    @JsonProperty("end_date")
    private LocalDate endDate;
    @NotNull
    @JsonProperty("company_id")
    private Long      companyId;
    @NotNull
    @JsonProperty("user_id")
    private Long      userId;
}
