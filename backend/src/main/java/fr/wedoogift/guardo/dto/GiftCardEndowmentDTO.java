package fr.wedoogift.guardo.dto;

import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.entities.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GiftCardEndowmentDTO {
    private List<Company>              companies;
    private List<User>                 users;
    private List<GiftCardDistribution> distributions;
}
