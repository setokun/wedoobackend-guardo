package fr.wedoogift.guardo.dto;

import fr.wedoogift.guardo.entities.WalletType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletDTO {
    private Long       id;
    private String     name;
    private WalletType type;
}
