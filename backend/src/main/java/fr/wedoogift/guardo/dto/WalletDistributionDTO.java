package fr.wedoogift.guardo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletDistributionDTO {
    @NotNull
    @JsonProperty("wallet_id")
    private Integer       walletId;
    @NotNull
    private Integer       amount;
    @JsonProperty("start_date")
    private LocalDate startDate;
    @JsonProperty("end_date")
    private LocalDate endDate;
    @NotNull
    @JsonProperty("company_id")
    private Long      companyId;
    @NotNull
    @JsonProperty("user_id")
    private Long      userId;
}
