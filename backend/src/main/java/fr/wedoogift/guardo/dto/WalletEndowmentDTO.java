package fr.wedoogift.guardo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.WalletDistribution;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletEndowmentDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<WalletDTO>          wallets;
    private List<Company>            companies;
    private List<WalletUserDTO>      users;
    private List<WalletDistribution> distributions;
}
