package fr.wedoogift.guardo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletUserDTO {
    private Long             id;
    private List<BalanceDTO> balance;
}
