package fr.wedoogift.guardo.entities;

import java.time.LocalDate;

public interface Distribution {
    int       getAmount();
    LocalDate getStartDate();
    LocalDate getEndDate();
    Long      getCompanyId();
    Long      getUserId();
}
