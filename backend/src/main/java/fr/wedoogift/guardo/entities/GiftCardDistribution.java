package fr.wedoogift.guardo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class GiftCardDistribution implements Distribution {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long      id;
    private int       amount;
    @JsonProperty("start_date")
    private LocalDate startDate;
    @JsonProperty("end_date")
    private LocalDate endDate;
    @JsonProperty("company_id")
    private Long      companyId;
    @JsonProperty("user_id")
    private Long      userId;

    public GiftCardDistribution(Long companyId, Long userId, LocalDate startDate, int amount) {
        this.companyId = companyId;
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = startDate.plusYears(1L).minusDays(1L);
        this.amount = amount;
    }
}
