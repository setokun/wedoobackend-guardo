package fr.wedoogift.guardo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.wedoogift.guardo.core.utils.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class WalletDistribution implements Distribution {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long      id;
    @JsonProperty("wallet_id")
    private int       walletId;
    private int       amount;
    @JsonProperty("start_date")
    private LocalDate startDate;
    @JsonProperty("end_date")
    private LocalDate endDate;
    @JsonProperty("company_id")
    private Long      companyId;
    @JsonProperty("user_id")
    private Long      userId;

    public WalletDistribution(Long companyId, Long userId, WalletType walletType, LocalDate startDate, int amount) {
        this.walletId = walletType.value;
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = DateUtils.getWalletEndDate(walletType, startDate);
        this.companyId = companyId;
        this.userId = userId;
    }
}
