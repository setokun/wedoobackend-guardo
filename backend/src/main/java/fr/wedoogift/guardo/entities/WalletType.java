package fr.wedoogift.guardo.entities;

public enum WalletType {
    GIFT(1),
    FOOD(2);

    public final int value;

    WalletType(int value) {
        this.value = value;
    }

    public static WalletType fromInteger(int x) {
        return switch (x) {
            case 1 -> GIFT;
            case 2 -> FOOD;
            default -> null;
        };
    }
}
