package fr.wedoogift.guardo.repositories;

import fr.wedoogift.guardo.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompaniesRepository extends JpaRepository<Company, Long> {
}
