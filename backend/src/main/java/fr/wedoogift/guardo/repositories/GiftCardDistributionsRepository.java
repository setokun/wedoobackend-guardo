package fr.wedoogift.guardo.repositories;

import fr.wedoogift.guardo.entities.GiftCardDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GiftCardDistributionsRepository extends JpaRepository<GiftCardDistribution, Long> {
    List<GiftCardDistribution> findAllByCompanyId(Long companyId);

    List<GiftCardDistribution> findAllByUserId(Long userId);
}
