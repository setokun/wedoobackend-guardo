package fr.wedoogift.guardo.repositories;

import fr.wedoogift.guardo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {
}
