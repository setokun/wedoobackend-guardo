package fr.wedoogift.guardo.repositories;

import fr.wedoogift.guardo.entities.WalletDistribution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface WalletDistributionsRepository extends JpaRepository<WalletDistribution, Long> {

    List<WalletDistribution> findAllByCompanyId(final Long companyId);

    @Query(value = """
            SELECT SUM(amount) 
            FROM WalletDistribution 
            WHERE userId   = :userId 
              AND walletId = :walletId
              AND endDate >= :endDate
            """)
    Integer findAmountByWalletIdAndUserId(final int walletId, final Long userId, final LocalDate endDate);
}
