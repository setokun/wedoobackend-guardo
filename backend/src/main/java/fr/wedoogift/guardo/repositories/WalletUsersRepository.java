package fr.wedoogift.guardo.repositories;

import fr.wedoogift.guardo.entities.WalletUser;
import fr.wedoogift.guardo.entities.WalletUserId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WalletUsersRepository extends JpaRepository<WalletUser, WalletUserId> {

    List<WalletUser> findAllByIdUserId(Long userId);

    @Query(value = "SELECT SUM(amount) FROM WalletUser WHERE id.userId = :userId AND id.walletId = :walletId")
    Integer findBalanceByWalletIdAndUserId(int walletId, Long userId);
}
