package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.dto.GiftCardEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletEndowmentDTO;
import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.entities.WalletDistribution;

import java.util.List;
import java.util.NoSuchElementException;

public interface CompaniesService {

    String AMOUNT_MUST_BE_POSITIVE       = "Amount must be positive";
    String COMPANY_BALANCE_IS_NOT_ENOUGH = "Company balance is not enough";

    List<Company> saveAll(final List<Company> companies);

    void deleteAll();

    GiftCardEndowmentDTO distributeGiftCards(List<GiftCardDistribution> giftCardDistributions);

    WalletEndowmentDTO distributeWallets(List<WalletDistribution> walletDistributions);

    int getCurrentBalance(Long companyId) throws NoSuchElementException;
}
