package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.dto.GiftCardEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletEndowmentDTO;
import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.Distribution;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.entities.WalletDistribution;
import fr.wedoogift.guardo.repositories.CompaniesRepository;
import fr.wedoogift.guardo.repositories.GiftCardDistributionsRepository;
import fr.wedoogift.guardo.repositories.WalletDistributionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CompaniesServiceImpl implements CompaniesService {

    private final CompaniesRepository             companiesRepository;
    private final GiftCardDistributionsRepository giftCardDistributionsRepository;
    private final WalletDistributionsRepository   walletDistributionsRepository;
    private final UsersService                    usersService;
    private final GiftCardService                 giftCardService;
    private final WalletService                   walletService;

    @Override
    public List<Company> saveAll(List<Company> companies) {
        return companiesRepository.saveAll(companies);
    }

    @Override
    public void deleteAll() {
        companiesRepository.deleteAll();
    }

    @Override
    public WalletEndowmentDTO distributeWallets(List<WalletDistribution> walletDistributions) {
        // WeDooFailFast
        checkAmounts(walletDistributions);
        checkCompaniesBalances(walletDistributions);

        List<Long> companyIds = walletDistributions.stream()
                                                   .map(WalletDistribution::getCompanyId)
                                                   .distinct()
                                                   .toList();
        List<Long> userIds = walletDistributions.stream()
                                                .map(WalletDistribution::getUserId)
                                                .distinct()
                                                .toList();

        List<WalletDistribution> distributedWallets = walletService.saveAll(walletDistributions);

        return WalletEndowmentDTO.builder()
                                 .companies(companyIds.stream()
                                                      .map(id -> companiesRepository.findById(id).orElseThrow(NoSuchElementException::new))
                                                      .peek(c -> c.setBalance(getCurrentBalance(c.getId()))).toList())
                                 .users(userIds.stream()
                                               .map(usersService::getCurrentWalletBalance)
                                               .toList())
                                 .distributions(distributedWallets)
                                 .build();
    }

    @Override
    public GiftCardEndowmentDTO distributeGiftCards(List<GiftCardDistribution> giftCardDistributions) {
        // WeDooFailFast
        checkAmounts(giftCardDistributions);
        checkCompaniesBalances(giftCardDistributions);

        List<Long> companyIds = giftCardDistributions.stream().map(GiftCardDistribution::getCompanyId).distinct().toList();
        List<Long> userIds    = giftCardDistributions.stream().map(GiftCardDistribution::getUserId).distinct().toList();

        List<GiftCardDistribution> distributedGiftCards = giftCardService.saveAll(giftCardDistributions);

        return GiftCardEndowmentDTO.builder()
                                   .companies(companyIds.stream()
                                                        .map(id -> companiesRepository.findById(id).orElseThrow(NoSuchElementException::new))
                                                        .peek(c -> c.setBalance(getCurrentBalance(c.getId()))).toList())
                                   .users(userIds.stream()
                                                 .map(id -> usersService.findById(id).orElseThrow(NoSuchElementException::new))
                                                 .peek(u -> u.setBalance(usersService.getCurrentBalance(u.getId()))).toList())
                                   .distributions(distributedGiftCards)
                                   .build();
    }

    @Override
    public int getCurrentBalance(Long companyId) throws NoSuchElementException {
        Company company = companiesRepository.findById(companyId)
                                             .orElseThrow(NoSuchElementException::new);

        return company.getBalance()
                       - getDistributedGiftCardsAmount(companyId)
                       - getDistributedWalletsAmount(companyId);
    }

    private int getDistributedWalletsAmount(Long companyId) {
        return walletDistributionsRepository.findAllByCompanyId(companyId)
                                            .stream()
                                            .mapToInt(WalletDistribution::getAmount)
                                            .sum();
    }

    private int getDistributedGiftCardsAmount(Long companyId) {
        return giftCardDistributionsRepository.findAllByCompanyId(companyId)
                                              .stream()
                                              .mapToInt(GiftCardDistribution::getAmount)
                                              .sum();
    }

    private <T extends Distribution> void checkAmounts(List<T> distributions) {
        // WeDooVerification that no distribution amount is negative
        if (distributions.stream().anyMatch(wd -> wd.getAmount() < 0)) {
            throw new NumberFormatException(AMOUNT_MUST_BE_POSITIVE);
        }
    }

    private <T extends Distribution> void checkCompaniesBalances(List<T> distributions) {
        // WeDooVerification that each company balance is sufficient
        distributions.stream()
                     .collect(Collectors.groupingBy(Distribution::getCompanyId,
                                                    Collectors.summingInt(Distribution::getAmount)))
                     .forEach((companyId, amountSum) -> {
                         if (getCurrentBalance(companyId) - amountSum < 0) {
                             throw new NumberFormatException(COMPANY_BALANCE_IS_NOT_ENOUGH);
                         }
                     });
    }
}
