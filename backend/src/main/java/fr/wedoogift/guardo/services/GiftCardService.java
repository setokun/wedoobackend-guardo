package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.entities.GiftCardDistribution;

import java.util.List;

public interface GiftCardService {
    List<GiftCardDistribution> findAllByCompanyId(Long companyId);

    List<GiftCardDistribution> saveAll(final List<GiftCardDistribution> giftCardDistributions);

    void deleteAllGiftCards();
}
