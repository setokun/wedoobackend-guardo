package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.repositories.GiftCardDistributionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GiftCardServiceImpl implements GiftCardService {

    private final GiftCardDistributionsRepository giftCardDistributionsRepository;

    @Override
    public List<GiftCardDistribution> findAllByCompanyId(Long companyId) {
        return giftCardDistributionsRepository.findAllByCompanyId(companyId);

    }

    @Override
    public List<GiftCardDistribution> saveAll(List<GiftCardDistribution> giftCardDistributions) {
        return giftCardDistributionsRepository.saveAll(giftCardDistributions);

    }

    @Override
    public void deleteAllGiftCards() {
        giftCardDistributionsRepository.deleteAll();
    }

}
