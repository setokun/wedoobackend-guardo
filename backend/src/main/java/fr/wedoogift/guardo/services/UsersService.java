package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.dto.WalletUserDTO;
import fr.wedoogift.guardo.entities.User;
import fr.wedoogift.guardo.entities.WalletUser;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public interface UsersService {
    int getCurrentBalance(Long userId) throws NoSuchElementException;

    void saveAll(final List<User> users);

    void deleteAll();

    void saveAllWalletUsers(final List<WalletUser> users);

    WalletUserDTO getCurrentWalletBalance(final Long userId);

    Optional<User> findById(final Long id);
}
