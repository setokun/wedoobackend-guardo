package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.dto.BalanceDTO;
import fr.wedoogift.guardo.dto.WalletUserDTO;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.entities.User;
import fr.wedoogift.guardo.entities.WalletType;
import fr.wedoogift.guardo.entities.WalletUser;
import fr.wedoogift.guardo.repositories.GiftCardDistributionsRepository;
import fr.wedoogift.guardo.repositories.UsersRepository;
import fr.wedoogift.guardo.repositories.WalletDistributionsRepository;
import fr.wedoogift.guardo.repositories.WalletUsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository                 usersRepository;
    private final WalletUsersRepository           walletUsersRepository;
    private final GiftCardDistributionsRepository giftCardDistributionsRepository;
    private final WalletDistributionsRepository   walletDistributionsRepository;

    @Override
    public int getCurrentBalance(Long userId) throws NoSuchElementException {
        User user = usersRepository.findById(userId)
                                   .orElseThrow(NoSuchElementException::new);

        int endowedAmount = giftCardDistributionsRepository.findAllByUserId(userId)
                                                           .stream()
                                                           // WeDooCheck GiftCard is still valid
                                                           .filter(d -> d.getEndDate() != null && !d.getEndDate().isBefore(LocalDate.now()))
                                                           .mapToInt(GiftCardDistribution::getAmount)
                                                           .sum();

        return user.getBalance() + endowedAmount;
    }

    @Override
    public void saveAll(List<User> users) {
        usersRepository.saveAll(users);
    }

    @Override
    public void deleteAll() {
        usersRepository.deleteAll();
    }

    @Override
    public void saveAllWalletUsers(List<WalletUser> users) {
        walletUsersRepository.saveAll(users);
    }

    @Override
    public WalletUserDTO getCurrentWalletBalance(Long userId) {

        List<BalanceDTO> walletUsers = Stream.of(WalletType.values())
                                             .map(type -> BalanceDTO.builder()
                                                                    .walletId(type.value)
                                                                    .amount(getAmount(userId, type))
                                                                    .build())
                                             .filter(b -> b.getAmount() > 0)
                                             .toList();

        return WalletUserDTO.builder()
                            .id(userId)
                            .balance(walletUsers)
                            .build();


    }

    @Override
    public Optional<User> findById(Long userId) {
        return usersRepository.findById(userId);
    }

    private int getAmount(final Long userId, final WalletType type) {
        Integer balanceByWalletIdAndUserId = walletUsersRepository.findBalanceByWalletIdAndUserId(type.value, userId);
        Integer amountByWalletIdAndUserId  = walletDistributionsRepository.findAmountByWalletIdAndUserId(type.value, userId, LocalDate.now());

        return (balanceByWalletIdAndUserId != null ? balanceByWalletIdAndUserId : 0)
                       + (amountByWalletIdAndUserId != null ? amountByWalletIdAndUserId : 0);
    }
}
