package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.entities.WalletDistribution;

import java.util.List;

public interface WalletService {
    List<WalletDistribution> saveAll(final List<WalletDistribution> walletDistributions);

    void deleteAllWallets();
}
