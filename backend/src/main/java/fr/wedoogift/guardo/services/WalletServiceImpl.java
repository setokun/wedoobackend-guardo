package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.entities.WalletDistribution;
import fr.wedoogift.guardo.repositories.WalletDistributionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletDistributionsRepository walletDistributionsRepository;

    @Override
    public List<WalletDistribution> saveAll(List<WalletDistribution> walletDistributions) {
        return walletDistributionsRepository.saveAll(walletDistributions);
    }

    @Override
    public void deleteAllWallets() {
        walletDistributionsRepository.deleteAll();
    }
}
