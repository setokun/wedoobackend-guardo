package fr.wedoogift.guardo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.wedoogift.guardo.dto.GiftCardEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletUserDTO;
import fr.wedoogift.guardo.entities.*;
import fr.wedoogift.guardo.services.CompaniesService;
import fr.wedoogift.guardo.services.GiftCardService;
import fr.wedoogift.guardo.services.UsersService;
import fr.wedoogift.guardo.services.WalletService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class JsonTests {

    @Autowired
    GiftCardService  giftCardService;
    @Autowired
    WalletService    walletService;
    @Autowired
    UsersService     usersService;
    @Autowired
    CompaniesService companiesService;

    private final ObjectMapper objectMapper = getObjectMapper();

    @Test
    void distributeGiftCards() {

        try {
            GiftCardEndowmentDTO giftCardEndowmentDTO = objectMapper.readValue(new File("Level1/data/input.json"), GiftCardEndowmentDTO.class);

            initLevel1Context(giftCardEndowmentDTO);

            GiftCardEndowmentDTO distributedGiftCard = companiesService.distributeGiftCards(List.of(
                    new GiftCardDistribution(1L, 1L, LocalDate.of(2021, Month.SEPTEMBER, 16), 50),
                    new GiftCardDistribution(1L, 2L, LocalDate.of(2021, Month.AUGUST, 1), 100),
                    new GiftCardDistribution(2L, 3L, LocalDate.of(2021, Month.MAY, 1), 1000)));


            File outputFile = new File("Level1/data/output.json");
            //noinspection ResultOfMethodCallIgnored
            outputFile.createNewFile();

            // WeDooConversion of endowment object to JSON file
            objectMapper.writeValue(outputFile, distributedGiftCard);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    private void initLevel1Context(GiftCardEndowmentDTO giftCardEndowmentDTO) {
        companiesService.saveAll(giftCardEndowmentDTO.getCompanies());
        usersService.saveAll(giftCardEndowmentDTO.getUsers());
        giftCardService.saveAll(giftCardEndowmentDTO.getDistributions());
    }

    private void initLevel2Context(WalletEndowmentDTO walletEndowmentDTO) {
        companiesService.saveAll(walletEndowmentDTO.getCompanies());
        usersService.saveAllWalletUsers(getWalletUsersToSave(walletEndowmentDTO.getUsers()));
        walletService.saveAll(walletEndowmentDTO.getDistributions());
    }

    private List<WalletUser> getWalletUsersToSave(List<WalletUserDTO> walletUserDTOs) {
        return walletUserDTOs.stream()
                             .map(wu -> wu.getBalance()
                                          .stream()
                                          .map(b -> WalletUser.builder()
                                                              .id(WalletUserId.builder()
                                                                              .userId(wu.getId())
                                                                              .walletId(b.getWalletId())
                                                                              .build())
                                                              .amount(b.getAmount())
                                                              .build())
                                          .toList())
                             .flatMap(List::stream)
                             .toList();
    }

    @Test
    void distributeWallets() {

        try {
            WalletEndowmentDTO walletEndowmentDTO = objectMapper.readValue(new File("Level2/data/input.json"), WalletEndowmentDTO.class);

            initLevel2Context(walletEndowmentDTO);

            // WeDooLittleCheat on years, because if we not, no users would have wallets on output (all example cards are too old)
            WalletEndowmentDTO distributedWallets = companiesService.distributeWallets(List.of(
                    new WalletDistribution(1L, 1L, WalletType.GIFT, LocalDate.of(2021, Month.SEPTEMBER, 16), 50),
                    new WalletDistribution(1L, 2L, WalletType.GIFT, LocalDate.of(2021, Month.AUGUST, 1), 100),
                    new WalletDistribution(2L, 3L, WalletType.GIFT, LocalDate.of(2021, Month.MAY, 1), 1000),
                    new WalletDistribution(1L, 1L, WalletType.FOOD, LocalDate.of(2021, Month.MAY, 1), 250)));


            File outputFile = new File("Level2/data/output.json");
            //noinspection ResultOfMethodCallIgnored
            outputFile.createNewFile();

            // WeDooConversion of endowment object to JSON file
            objectMapper.writeValue(outputFile, distributedWallets);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        return objectMapper;
    }


}
