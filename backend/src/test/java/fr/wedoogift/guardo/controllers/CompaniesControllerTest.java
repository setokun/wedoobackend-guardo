package fr.wedoogift.guardo.controllers;

import fr.wedoogift.guardo.dto.GiftCardDistributionDTO;
import fr.wedoogift.guardo.dto.GiftCardEndowmentDTO;
import fr.wedoogift.guardo.dto.WalletDistributionDTO;
import fr.wedoogift.guardo.dto.WalletEndowmentDTO;
import fr.wedoogift.guardo.repositories.GiftCardDistributionsRepository;
import fr.wedoogift.guardo.repositories.UsersRepository;
import fr.wedoogift.guardo.repositories.WalletDistributionsRepository;
import fr.wedoogift.guardo.repositories.WalletUsersRepository;
import fr.wedoogift.guardo.services.CompaniesService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static fr.wedoogift.guardo.core.utils.TestUtils.asJsonString;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unused")
@RunWith(SpringRunner.class)
@WebMvcTest(CompaniesController.class)
@AutoConfigureMockMvc
@ComponentScan(basePackages = "fr.wedoogift.guardo")
class CompaniesControllerTest {

    @Autowired
    private MockMvc                         mvc;
    @MockBean
    private CompaniesService                companiesService;
    @MockBean
    private UsersRepository                 usersRepository;
    @MockBean
    private WalletUsersRepository           walletUsersRepository;
    @MockBean
    private GiftCardDistributionsRepository giftCardDistributionsRepository;
    @MockBean
    private WalletDistributionsRepository   walletDistributionsRepository;

    @Test
    @WithMockUser(roles = "ADMIN")
    void distributeGiftCards() throws Exception {

        when(companiesService.distributeGiftCards(anyList())).thenReturn(GiftCardEndowmentDTO.builder().build());

        mvc.perform(MockMvcRequestBuilders
                            .post("/companies/distributeGiftCards")
                            .accept(MediaType.APPLICATION_JSON)
                            .content(asJsonString(List.of(GiftCardDistributionDTO.builder()
                                                                                 .amount(50)
                                                                                 .companyId(1L)
                                                                                 .userId(1L)
                                                                                 .build())))
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void distributeWallets() throws Exception {

        when(companiesService.distributeWallets(anyList())).thenReturn(WalletEndowmentDTO.builder().build());

        mvc.perform(MockMvcRequestBuilders
                            .post("/companies/distributeWallets")
                            .accept(MediaType.APPLICATION_JSON)
                            .content(asJsonString(List.of(WalletDistributionDTO.builder()
                                                                               .walletId(1)
                                                                               .amount(50)
                                                                               .companyId(1L)
                                                                               .userId(1L)
                                                                               .build())))
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getBalance() throws Exception {
        final int amount = 800;

        when(companiesService.getCurrentBalance(1L)).thenReturn(amount);

        mvc.perform(MockMvcRequestBuilders
                            .get("/companies/{id}/balance", 1)
                            .accept(MediaType.APPLICATION_JSON))
           .andDo(print())
           .andExpect(status().isOk())
           .andExpect(MockMvcResultMatchers.jsonPath("$").value(amount));

    }

    @Test
    @WithMockUser(roles = "USER")
    void getBalanceByUserShouldRaise403() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                            .get("/companies/{id}/balance", 1)
                            .accept(MediaType.APPLICATION_JSON))
           .andDo(print())
           .andExpect(status().isForbidden());

    }
}
