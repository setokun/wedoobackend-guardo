package fr.wedoogift.guardo.controllers;

import fr.wedoogift.guardo.repositories.*;
import fr.wedoogift.guardo.services.UsersService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unused")
@RunWith(SpringRunner.class)
@WebMvcTest(UsersController.class)
@AutoConfigureMockMvc
@ComponentScan(basePackages = "fr.wedoogift.guardo")
class UsersControllerTest {

    @Autowired
    private MockMvc                         mvc;
    @MockBean
    private UsersService                    usersService;
    @MockBean
    private CompaniesRepository             companiesRepository;
    @MockBean
    private WalletUsersRepository           walletUsersRepository;
    @MockBean
    private GiftCardDistributionsRepository giftCardDistributionsRepository;
    @MockBean
    private WalletDistributionsRepository   walletDistributionsRepository;

    @Test
    @WithMockUser(roles = "USER")
    void getBalance() throws Exception {
        final int amount = 200;

        when(usersService.getCurrentBalance(1L)).thenReturn(amount);

        mvc.perform(MockMvcRequestBuilders
                            .get("/users/{id}/balance", 1)
                            .accept(MediaType.APPLICATION_JSON))
           .andDo(print())
           .andExpect(status().isOk())
           .andExpect(MockMvcResultMatchers.jsonPath("$").value(amount));
    }

    @Test
    @WithMockUser(roles = "COMPANY")
    void getBalanceByUserShouldRaise403() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                            .get("/users/{id}/balance", 1)
                            .accept(MediaType.APPLICATION_JSON))
           .andDo(print())
           .andExpect(status().isForbidden());

    }
}
