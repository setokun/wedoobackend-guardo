package fr.wedoogift.guardo.core.utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateUtilsTest {

    @Test
    void getNextYearFebruaryEnd() {
        LocalDate expected = LocalDate.of(2021, Month.FEBRUARY, 28);

        assertEquals(expected, DateUtils.getNextYearFebruaryEnd(LocalDate.of(2020, Month.JANUARY, 1)));
        assertEquals(expected, DateUtils.getNextYearFebruaryEnd(LocalDate.of(2020, Month.JANUARY, 15)));
        assertEquals(expected, DateUtils.getNextYearFebruaryEnd(LocalDate.of(2020, Month.FEBRUARY, 29)));
        assertEquals(expected, DateUtils.getNextYearFebruaryEnd(LocalDate.of(2020, Month.MARCH, 1)));
    }
}
