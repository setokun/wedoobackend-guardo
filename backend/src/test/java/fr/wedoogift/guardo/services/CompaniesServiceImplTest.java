package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.entities.Company;
import fr.wedoogift.guardo.entities.GiftCardDistribution;
import fr.wedoogift.guardo.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

import static fr.wedoogift.guardo.services.CompaniesService.AMOUNT_MUST_BE_POSITIVE;
import static fr.wedoogift.guardo.services.CompaniesService.COMPANY_BALANCE_IS_NOT_ENOUGH;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CompaniesServiceImplTest {

    @Autowired
    UsersService     usersService;
    @Autowired
    CompaniesService companiesService;
    @Autowired
    GiftCardService  giftCardService;

    private final int COMPANY_3_AMOUNT = 3000;

    @BeforeEach
    void setUp() {
        // WeDooCleanUp
        companiesService.deleteAll();
        usersService.deleteAll();
        giftCardService.deleteAllGiftCards();

        // WeDooMock data insertion
        companiesService.saveAll(List.of(
                Company.builder().id(1L).name("WeDooOne").balance(1000).build(),
                Company.builder().id(2L).name("WeDooTwo").balance(2000).build(),
                Company.builder().id(3L).name("WeDooThree").balance(COMPANY_3_AMOUNT).build()
        ));

        giftCardService.saveAll(List.of(GiftCardDistribution.builder().companyId(1L).userId(1L).amount(300).build(),
                                        GiftCardDistribution.builder().companyId(1L).userId(1L).amount(50).build(),
                                        GiftCardDistribution.builder().companyId(2L).userId(1L).amount(300).build()));

        usersService.saveAll(List.of(
                User.builder().id(1L).balance(100).build(),
                User.builder().id(2L).balance(0).build()
        ));
    }

    @Test
    void getCurrentBalance() {
        int currentBalance = companiesService.getCurrentBalance(1L);

        assertEquals(650, currentBalance);
    }

    @Test
    void get_current_balance_should_raise_exception_on_unknown_company_id() {
        assertThrows(NoSuchElementException.class,
                     () -> companiesService.getCurrentBalance(10L));
    }

    @Test
    void distributeGiftCard() {
        companiesService.distributeGiftCards(List.of(new GiftCardDistribution(3L, 1L, LocalDate.now(), 200),
                                                     new GiftCardDistribution(2L, 2L, LocalDate.now(), 100)));

        List<GiftCardDistribution> company3GiftCardDistributions = giftCardService.findAllByCompanyId(3L);
        assertEquals(1, company3GiftCardDistributions.size());
        assertEquals(LocalDate.now().plusYears(1).minusDays(1), company3GiftCardDistributions.get(0).getEndDate());

        List<GiftCardDistribution> company2GiftCardDistributions = giftCardService.findAllByCompanyId(2L);
        assertEquals(2, company2GiftCardDistributions.size());
        assertEquals(100, company2GiftCardDistributions.get(1).getAmount());
    }

    @Test
    void distribute_gift_card_should_raise_exception_on_negative_amount() {
        Exception exception = assertThrows(NumberFormatException.class,
                                           () -> companiesService.distributeGiftCards(List.of(new GiftCardDistribution(3L, 1L, LocalDate.now(), -1))));

        assertTrue(exception.getMessage().contains(AMOUNT_MUST_BE_POSITIVE));
    }

    @Test
    void distribute_gift_card_should_raise_exception_on_insufficient_company_balance() {
        Exception exception = assertThrows(NumberFormatException.class,
                                           () -> companiesService.distributeGiftCards(List.of(new GiftCardDistribution(3L, 1L, LocalDate.now(), COMPANY_3_AMOUNT + 1))));

        assertTrue(exception.getMessage().contains(COMPANY_BALANCE_IS_NOT_ENOUGH));
    }

    @Test
    void findAllByCompanyId() {
        companiesService.distributeGiftCards(List.of(new GiftCardDistribution(3L, 1L, LocalDate.now(), 200),
                                                     new GiftCardDistribution(3L, 2L, LocalDate.now(), 200),
                                                     new GiftCardDistribution(2L, 2L, LocalDate.now(), 100)));

        List<GiftCardDistribution> giftCardDistributions = giftCardService.findAllByCompanyId(3L);

        assertEquals(2, giftCardDistributions.size());
    }
}
