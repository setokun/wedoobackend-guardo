package fr.wedoogift.guardo.services;

import fr.wedoogift.guardo.dto.BalanceDTO;
import fr.wedoogift.guardo.dto.WalletUserDTO;
import fr.wedoogift.guardo.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UsersServiceImplTest {

    @Autowired
    UsersService    usersService;
    @Autowired
    GiftCardService giftCardService;
    @Autowired
    WalletService   walletService;

    @BeforeEach
    void setUp() {
        // WeDooCleanUp
        usersService.deleteAll();
        giftCardService.deleteAllGiftCards();
        walletService.deleteAllWallets();

        // WeDooMock data insertion
        usersService.saveAll(List.of(
                User.builder().id(1L).balance(100).build(),
                User.builder().id(2L).balance(0).build()
        ));

        usersService.saveAllWalletUsers(List.of(
                WalletUser.builder().id(WalletUserId.builder().userId(1L).walletId(1).build()).amount(50).build(),
                WalletUser.builder().id(WalletUserId.builder().userId(1L).walletId(2).build()).amount(100).build(),
                WalletUser.builder().id(WalletUserId.builder().userId(2L).walletId(1).build()).amount(250).build()));

        giftCardService.saveAll(List.of(
                GiftCardDistribution.builder().id(1L).userId(1L).amount(100).startDate(LocalDate.now().minusYears(2)).endDate(LocalDate.now().minusYears(1)).build(),
                GiftCardDistribution.builder().id(2L).userId(1L).amount(200).startDate(LocalDate.now().minusYears(1)).endDate(LocalDate.now()).build(),
                GiftCardDistribution.builder().id(3L).userId(1L).amount(200).startDate(LocalDate.now().minusYears(1)).endDate(LocalDate.now().plusDays(1)).build(),
                GiftCardDistribution.builder().id(4L).userId(2L).amount(300).startDate(LocalDate.now().minusYears(1)).endDate(LocalDate.now()).build()
        ));

        walletService.saveAll(List.of(
                new WalletDistribution(1L, 1L, WalletType.GIFT, LocalDate.now(), 50),
                new WalletDistribution(1L, 2L, WalletType.GIFT, LocalDate.now(), 350),
                new WalletDistribution(2L, 1L, WalletType.FOOD, LocalDate.now(), 650)
        ));
    }

    @Test
    void getCurrentBalance() {
        assertEquals(500, usersService.getCurrentBalance(1L));
        assertEquals(300, usersService.getCurrentBalance(2L));
    }

    @Test
    void get_current_balance_should_raise_exception_on_unknown_user_id() {
        assertThrows(NoSuchElementException.class, () -> usersService.getCurrentBalance(1000L));
    }

    @Test
    void getCurrentWalletBalance() {

        assertEquals(WalletUserDTO.builder()
                                  .id(1L)
                                  .balance(List.of(BalanceDTO.builder().walletId(1).amount(100).build(),
                                                   BalanceDTO.builder().walletId(2).amount(750).build()))
                                  .build(), usersService.getCurrentWalletBalance(1L));

        assertEquals(WalletUserDTO.builder()
                                  .id(2L)
                                  .balance(List.of(BalanceDTO.builder().walletId(1).amount(600).build()))
                                  .build(), usersService.getCurrentWalletBalance(2L));
    }

    @Test
    void findById() {
        assertTrue(usersService.findById(1L).isPresent());
        assertFalse(usersService.findById(1000L).isPresent());
    }
}
